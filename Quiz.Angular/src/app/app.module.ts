import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';



//Material Modules
import {
  MatButtonModule, MatCheckboxModule, MatInputModule, MatCardModule,
  MatListModule, MatToolbarModule, MatFormFieldModule, MatExpansionModule,
  MatRadioModule, MatDialogModule
} from '@angular/material';

// App Modules
import { AppComponent } from './app.component';
import { QuestionComponent } from './question/question.component';
import { ApiService } from './api.service';
import { AuthService } from './auth.service';
import { AuthInterceptor } from './auth.interceptor';
import { QuestionsComponent } from './questions/questions.component';
import { HomeComponent } from './home/home.component';
import { NavComponent } from './nav/nav.component';
import { QuizComponent } from './quiz/quiz.component';
import { QuizzesComponent } from './quizzes/quizzes.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { PlayComponent } from './play/play.component';
import { PlayQuizComponent } from './play-quiz/play-quiz.component';
import { FinishedComponent } from './finished/finished.component';

// Routes
const routes = [
  {path: '', component: HomeComponent},
  {path: 'home', component: HomeComponent},  
  {path: 'question', component: QuestionComponent},
  {path: 'question/:quizId', component: QuestionComponent},  
  {path: 'register', component: RegisterComponent},
  {path: 'login', component: LoginComponent},
  {path: 'quiz', component: QuizComponent},
  {path: 'play', component: PlayComponent},
  {path: 'playQuiz/:quizId', component: PlayQuizComponent}
]

@NgModule({
  declarations: [
    AppComponent, QuestionComponent, QuestionsComponent, HomeComponent, 
    NavComponent, QuizComponent, QuizzesComponent, RegisterComponent, LoginComponent, 
    PlayComponent, PlayQuizComponent, FinishedComponent
  ],
  imports: [
    BrowserModule,  FormsModule, HttpClientModule,  BrowserAnimationsModule,
    RouterModule.forRoot(routes), ReactiveFormsModule,

    MatButtonModule, MatCheckboxModule, MatInputModule, MatCardModule, MatListModule,
    MatToolbarModule, MatExpansionModule, MatRadioModule, MatDialogModule
  ],
  providers: [
      ApiService , AuthService, 
      { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
    ],
  bootstrap: [AppComponent],
  entryComponents:[ FinishedComponent ]
})
export class AppModule { }
