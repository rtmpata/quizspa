import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'home',
  template: '<quiz></quiz><quizzes></quizzes>',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
