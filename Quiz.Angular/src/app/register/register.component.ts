import { Component } from '@angular/core';
import { FormBuilder, Validators } from "@angular/forms";
import { AuthService } from '../auth.service';

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {

  form;
  constructor(private auth:AuthService, private fb: FormBuilder) { 
    this.form = fb.group({
      email: ['', Validators.required],
      password: ['',Validators.required]
    })
  }




}
